"""
coding-challenge-backend:engine.api
"""

import asyncio

from aiohttp import web

from engine.storage import Database
from engine.data_scanner import DataScanner
from engine import responsers
from core import settings


class Engine:
    _database = None

    _api_host = None
    _api_port = None
    _app = None

    def __init__(self, api_host, api_port):
        self._api_host = api_host
        self._api_port = api_port

    def launch(self):
        loop = asyncio.get_event_loop()

        self._open_connection()

        self._app = web.Application()
        self._setup_routes()

        try:
            print('Server running at [{0}:{1}]...'.format(
                self._api_host, self._api_port)
            )
            # Launches the REST API server
            web.run_app(self._app, host=self._api_host, port=self._api_port)
        except KeyboardInterrupt:
            pass
        finally:
            # Exit
            self._close_connection()
            loop.close()

    def _open_connection(self):
        self._database = Database()

        loop = asyncio.get_event_loop()
        loop.run_until_complete(self._database.connected())

    def _close_connection(self):
        if self._database:
            loop = asyncio.get_event_loop()
            loop.run_until_complete(self._database.disconnected())

    def _setup_routes(self):
        self._app.router.add_get('/api/v1/repositories', self._repositories)
        self._app.router.add_get('/api/v1/repositories/', self._repositories)

    async def _repositories(self, request):
        """
        Handles GET requests:
            * /api/v1/repositories
            * /api/v1/repositories/:page:size:sort_by_stars
        """
        params = dict(request.query)

        page = params.get('page', None)
        size = params.get('size', None)
        sort_by_stars = params.get('sort_by_stars', None)

        data_scanner = DataScanner(
        ).with_sorting(sort_by_stars).with_pagination(page, size)

        responser = responsers.Repositories(page, size, sort_by_stars)

        # Pipeline: database -> data_scanner -> responser
        self._database.pipe(data_scanner).pipe(responser)
        await self._database.flow()  # run data flow

        return web.json_response(
            responser.as_json(), headers=settings.API_RESPONSE_HEADERS
        )


def run():
    Engine(api_host=settings.API_HOST, api_port=settings.API_PORT).launch()
