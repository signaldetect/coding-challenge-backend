"""
coding-challenge-backend:engine.data_scanner
"""

import rethinkdb
from rethinkdb.errors import ReqlOpFailedError

from core.data_stream import Stream
from core import settings


class DataScanner(Stream):
    TABLE_NAME = 'repositories'
    INDEX_FIELD = 'stargazers_count'

    _query = None

    def __init__(self):
        self._query = rethinkdb.table(self.TABLE_NAME)

    async def flow(self, db_conn):
        try:
            result = await self._query.run(db_conn)
        except ReqlOpFailedError:
            pass
        else:
            while (await result.fetch_next()):
                data = await result.next()
                await self._next_stream.flow(data)

    def with_sorting(self, sort_by_stars):
        ordering = None
        if sort_by_stars == 'asc':
            ordering = rethinkdb.asc
        elif sort_by_stars == 'desc':
            ordering = rethinkdb.desc

        if ordering:
            self._query = self._query.order_by(
                index=ordering(self.INDEX_FIELD)
            )

        return self

    def with_pagination(self, page, size):
        page = self._valid_page_number(page)
        size = self._valid_page_size(size)

        self._query = self._query.skip((page - 1) * size)
        self._query = self._query.limit(size)

        return self

    def _valid_page_number(self, page):
        """
        Returns the valid (`int`, `> 0`) page number
        """
        if not isinstance(page, int):
            try:
                page = int(page)
            except (TypeError, ValueError):
                page = 1

        return page if page > 0 else 1

    def _valid_page_size(self, size):
        """
        Returns the valid (`int`, `> 0`, `<= MAX_PAGE_SIZE`) page size
        """
        if not isinstance(size, int):
            try:
                size = int(size)
            except (TypeError, ValueError):
                size = settings.DEFAULT_PAGE_SIZE

        if size <= 0:
            size = settings.DEFAULT_PAGE_SIZE
        elif size > settings.MAX_PAGE_SIZE:
            size = settings.MAX_PAGE_SIZE

        return size
