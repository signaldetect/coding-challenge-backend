"""
coding-challenge-backend:engine.responsers
"""

from core.data_stream import Stream


class Repositories(Stream):
    FIELDS = (
        'id',
        'full_name',
        'html_url',
        'description',
        'stargazers_count',
        'language'
    )

    _result = None

    def __init__(self, page=None, size=None, sort_by_stars=None):
        self._result = []

    async def flow(self, data):
        if isinstance(data, dict):
            self._result.append(
                {
                    field: data.get(field, None) for field in self.FIELDS
                }
            )

    def as_json(self):
        return self._result
