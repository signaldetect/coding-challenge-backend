"""
coding-challenge-backend:engine.storage
"""

from core.storage import DatabaseStream


class Database(DatabaseStream):

    async def flow(self):
        await self._next_stream.flow(self._db_conn)
