"""
coding-challenge-backend:core.storage
"""

import rethinkdb
from rethinkdb.errors import ReqlOpFailedError

from core.data_stream import Stream
from core import settings


class DatabaseStream(Stream):
    _db_conn = None  # connection

    async def connected(self):
        """
        Creates a new DB connection for read/write operations
        """
        rethinkdb.set_loop_type('asyncio')

        while self._db_conn is None:
            try:
                self._db_conn = await rethinkdb.connect(
                    host=settings.DB_CONN_HOST, port=settings.DB_CONN_PORT
                )
            except ReqlOpFailedError:
                pass
            else:
                await self._setup_db()
                await self._setup_tables()

        return self

    async def disconnected(self):
        """
        Closes an open DB connection
        """
        if self._db_conn:
            try:
                await self._db_conn.close()
            except ReqlOpFailedError:
                pass

        return self

    async def _setup_db(self):
        """
        Creates a database if it doesn't exist
        """
        try:
            await rethinkdb.db_create(settings.DB_NAME).run(self._db_conn)

            print(
                'Database `{db_name}` setup completed.'.format(
                    db_name=settings.DB_NAME
                )
            )
        except ReqlOpFailedError:
            pass

        self._db_conn.use(settings.DB_NAME)

    async def _setup_tables(self):
        """
        Creates tables if they doesn't exist
        """
        for table in settings.DB_TABLES:
            try:
                await self._setup_table(
                    table_name=table.get('name', None),
                    index_field=table.get('index_field', None)
                )
            except ReqlOpFailedError:
                pass

    async def _setup_table(self, table_name, index_field):
        """
        Creates a table if it doesn't exist
        """
        if table_name:
            await rethinkdb.table_create(table_name).run(self._db_conn)

            print(
                'Table `{table_name}` creation completed.'.format(
                    table_name=table_name
                )
            )

        if index_field:
            query = rethinkdb.table(table_name)
            await query.index_create(index_field).run(self._db_conn)

            print(
                'Index for `{index_field}` creation completed.'.format(
                    index_field=index_field
                )
            )
