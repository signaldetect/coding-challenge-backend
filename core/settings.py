"""
coding-challenge-backend:core.settings
"""

# Storage

DB_CONN_HOST = 'storage'
DB_CONN_PORT = 28015

DB_NAME = 'github'
DB_TABLES = (
    {'name': 'repositories', 'index_field': 'stargazers_count'},
)

# GitHub API

GITHUB_API_URL = (
    'https://api.github.com/search/repositories'
    '?q=language:{language}+stars:%3E={min_stars}&per_page={page_size}'
)
GITHUB_API_TIMEOUT = 10  # 10 sec

GITHUB_DEFAULT_LANGUAGE = 'python'
GITHUB_DEFAULT_MIN_STARS = 0
GITHUB_DEFAULT_PAGE_SIZE = 10

# Application API

API_RESPONSE_HEADERS = {
    'Access-Control-Allow-Headers': (
        'Content-Type, Authorization, X-Auth-Token'
    ),
    'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS',
    'Access-Control-Allow-Origin': '*'
}

API_HOST = '0.0.0.0'
API_PORT = 8888

# Pagination

DEFAULT_PAGE_SIZE = 10
MAX_PAGE_SIZE = 1000
