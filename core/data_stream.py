"""
coding-challenge-backend:core.data_stream
"""


class Stream:
    _next_stream = None

    def pipe(self, stream):
        self._next_stream = stream

        return stream

    async def flow(self, *args, **kwargs):
        raise NotImplementedError
