"""
coding-challenge-backend:feeder.storage
"""

from rethinkdb.errors import ReqlOpFailedError

from core.storage import DatabaseStream


class Database(DatabaseStream):

    async def flow(self, query):
        try:
            await query.run(self._db_conn)
        except ReqlOpFailedError:
            pass
