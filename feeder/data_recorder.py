"""
coding-challenge-backend:feeder.data_recorder
"""

import rethinkdb
from rethinkdb.errors import ReqlOpFailedError

from core.data_stream import Stream


class DataRecorder(Stream):
    TABLE_NAME = 'repositories'

    _query = None
    _db_conn = None

    def __init__(self):
        self._query = rethinkdb.table(self.TABLE_NAME)

    async def flow(self, data):
        if isinstance(data, (dict, list)):
            try:
                query = self._query.insert(data)
                await self._next_stream.flow(query)
            except ReqlOpFailedError:
                pass
