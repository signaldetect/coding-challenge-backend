"""
coding-challenge-backend:feeder.cli
"""

from cement.core.foundation import CementApp
from cement.core.exc import FrameworkError

from feeder import controllers


class App(CementApp):
    class Meta:
        label = 'feeder'
        base_controller = 'base'
        handlers = [
            controllers.BaseController,
            controllers.ResourceController
        ]


def run():
    with App() as app:
        try:
            app.run()
        except FrameworkError as exc:
            print(exc)
            app.exit_code = 300
        finally:
            if app.debug:
                import traceback
                traceback.print_exc()
