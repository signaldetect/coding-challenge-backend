"""
coding-challenge-backend:feeder.resources
"""

import re

import aiohttp
import async_timeout

from core.data_stream import Stream
from core import settings


class Repositories(Stream):
    FIELDS = (
        'full_name',
        'html_url',
        'description',
        'stargazers_count',
        'language'
    )

    _language = None
    _min_stars = None
    _page_size = None

    def __init__(self, language=None, min_stars=None, page_size=None):
        self._language = language or settings.GITHUB_DEFAULT_LANGUAGE
        self._min_stars = min_stars or settings.GITHUB_DEFAULT_MIN_STARS
        self._page_size = page_size or settings.GITHUB_DEFAULT_PAGE_SIZE

    async def flow(self):
        async for result in self._feed():
            data = [
                {
                    field: item.get(field, None) for field in self.FIELDS
                }
                for item in result.get('items', [])
            ]
            await self._next_stream.flow(data)

    def _feed(self):
        url = settings.GITHUB_API_URL.format(
            language=self._language,
            min_stars=self._min_stars,
            page_size=self._page_size
        )

        return Feed(url)


class Feed:
    _session = None
    _url = None

    def __init__(self, url):
        self._url = url

    async def __aiter__(self):
        self._session = aiohttp.ClientSession()

        return self

    async def __anext__(self):
        if self._url:
            return await self._fetch()
        # Otherwise
        self._session.close()
        raise StopAsyncIteration

    async def _fetch(self):
        with async_timeout.timeout(settings.GITHUB_API_TIMEOUT):
            async with self._session.get(self._url) as response:
                link_header = response.headers.get('Link', '')
                next_link = re.findall(r'<([^>]+)>; rel="next"', link_header)
                self._url = next_link[0] if next_link else None

                return await response.json()
