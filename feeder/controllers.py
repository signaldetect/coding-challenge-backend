"""
coding-challenge-backend:feeder.controllers
"""

import asyncio

from cement.core.controller import CementBaseController
from cement.core.controller import expose

from feeder import services


class BaseController(CementBaseController):
    class Meta:
        label = 'base'
        description = 'Data feeding of info from GitHub'

    @expose(help='Base controller default command', hide=True)
    def default(self):
        print('Base controller')


class ResourceController(CementBaseController):
    class Meta:
        label = 'feed'
        description = 'Data feeding of resource from GitHub'
        stacked_on = 'base'
        stacked_type = 'nested'
        arguments = [
            (['-l', '--language'], {'help': 'specified language'}),
            (['-ms', '--min_stars'], {'help': 'minimum stars'}),
            (['-ps', '--page_size'], {'help': 'one page size'})
        ]

    @expose(
        help='Data feeding of resource from GitHub default command',
        hide=True
    )
    def default(self):
        print('Data feeding of resource from GitHub')

    @expose(
        aliases=['repositories'],
        aliases_only=True,
        help='Data feeding of repositories from GitHub'
    )
    def feed_repositories(self):
        language = self.app.pargs.language
        min_stars = self.app.pargs.min_stars
        page_size = self.app.pargs.page_size

        loop = asyncio.get_event_loop()
        loop.run_until_complete(
            services.feed_repositories(language, min_stars, page_size)
        )
