"""
coding-challenge-backend:feeder.services
"""

from feeder import resources
from feeder.data_recorder import DataRecorder
from feeder.storage import Database


async def feed_repositories(language, min_stars, page_size):
    resource = resources.Repositories(language, min_stars, page_size)

    data_recorder = DataRecorder()

    database = await Database().connected()

    # Pipeline: resource -> data_recorder -> database
    resource.pipe(data_recorder).pipe(database)
    await resource.flow()  # run data flow

    await database.disconnected()
